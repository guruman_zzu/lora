





/* 
 * File: sensors.c
 * Author: Jure Macerl
 * Comments: LoRa timers used for callback functions
 * Revision history: 1.0
 */


#include "timers.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

void TimerInit( TimerEvent_t *obj, void ( *callback )( void ) )
{
    obj->Timestamp = 0;
    obj->ReloadValue = 0;
    obj->IsRunning = false;
    obj->Callback = callback;
    obj->Next = NULL;
}